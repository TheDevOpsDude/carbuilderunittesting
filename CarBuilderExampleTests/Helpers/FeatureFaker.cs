﻿using System;
using System.Collections.Generic;
using Bogus;
using CarBuilderExample.Models;

namespace CarBuilderExampleTests.Helpers
{
    public class FeatureFaker
    {
        private Faker<Feature> featureGenerator;

        public FeatureFaker()
        {
        }

        public Feature GetFeature() {
            return featureGenerator.Generate();
        }

        public List<Feature> GetFeatures() {
            List<Feature> features = new List<Feature>(new Feature[] {
                new Feature() {
                    BasePriceModifier = 95.99,
                    FeatureName = "Power Windows"
                },
                new Feature() {
                    BasePriceModifier = 112.98,
                    FeatureName = "Power Locks"
                },
                new Feature() {
                    BasePriceModifier = 699.54,
                    FeatureName = "Cruise Control"
                }
            });

            return features;
        }
    }
}
