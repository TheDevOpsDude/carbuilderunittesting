﻿using System;
namespace CarBuilderExample.Models
{
    public class Transmission
    {
        public bool Automatic { get; set; }
        public int Gears { get; set; }

        public Transmission()
        {
        }
    }
}
