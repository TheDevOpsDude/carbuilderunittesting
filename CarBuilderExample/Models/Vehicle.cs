﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CarBuilderExample.Models
{
    public class Vehicle
    {
        public Engine Engine { get; set; }
        public Body Body { get; set; }
        public List<Feature> Features { get; set; }
        public double BasePrice { get; set; }
        public double FuelCapacity { get; set; }
        public double TotalPrice { get; private set; }

        public void CalculateTotalPrice() {
        }

        public Vehicle()
        {
        }
    }
}
