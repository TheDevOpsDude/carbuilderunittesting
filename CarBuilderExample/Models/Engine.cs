﻿using System;
namespace CarBuilderExample.Models
{
    public class Engine
    {
        public string Configuration { get; set; }
        public int PistonCount { get; set; }
        public float Displacement { get; set; }
        public float AspirationType { get; set; }

        public Engine()
        {
        }
    }
}
